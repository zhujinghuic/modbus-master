package com.main;

import java.io.IOException;

/**
 * modubs通讯接口 （tcp,rtu,ascii实现）
 * @author zhujh
 */
public interface ModbusClient {
    byte[] getResponse(byte[] dataSend, int length) throws IOException;
}
