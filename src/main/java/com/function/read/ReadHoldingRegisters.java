package com.function.read;

import com.constant.ExpectedEnum;
import com.constant.FuncitonCodeEnum;
import com.function.base.read.BaseReadRegisterFunctionAction;

public class ReadHoldingRegisters extends BaseReadRegisterFunctionAction {

//    protected static final int FUNCTION_CODE = 0x1;

    public ReadHoldingRegisters(int startAddress, int quantity) {
        this.startAddress = startAddress;
        this.quantity = quantity;
        functionCode = FuncitonCodeEnum.READ_HOLDING_REGISTERS.toInt();
        expected = ExpectedEnum.READ_HOLDING_REGISTERS_EXPECTED.getValue();
    }

}
