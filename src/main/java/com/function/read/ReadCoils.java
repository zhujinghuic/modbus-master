package com.function.read;

import com.constant.ExpectedEnum;
import com.constant.FuncitonCodeEnum;
import com.function.base.read.BaseReadStatusFunctionAction;

public class ReadCoils extends BaseReadStatusFunctionAction {

//    protected static final int FUNCTION_CODE = 0x1;

    public ReadCoils(int startAddress, int quantity) {
        this.startAddress = startAddress;
        this.quantity = quantity;
        this.functionCode = FuncitonCodeEnum.READ_COILS.toInt();
        this.expected = ExpectedEnum.READ_COILS_EXPECTED.getValue();
    }


}
