package com.function.read;

import com.constant.ExpectedEnum;
import com.constant.FuncitonCodeEnum;
import com.function.base.read.BaseReadRegisterFunctionAction;

public class ReadInputRegisters extends BaseReadRegisterFunctionAction {

//    protected static final int FUNCTION_CODE = 0x1;

    public ReadInputRegisters(int startAddress, int quantity) {
        this.startAddress = startAddress;
        this.quantity = quantity;
        functionCode = FuncitonCodeEnum.READ_INPUT_REGISTERS.toInt();
        expected = ExpectedEnum.READ_INPUT_REGISTERS_EXPECTED.getValue();
    }

}
