package com.function.read;

import com.constant.ExpectedEnum;
import com.constant.FuncitonCodeEnum;
import com.function.base.read.BaseReadStatusFunctionAction;

public class ReadDiscreteInputs extends BaseReadStatusFunctionAction {

//    protected static final int FUNCTION_CODE = 0x1;

    public ReadDiscreteInputs(int startAddress, int quantity) {
        this.startAddress = startAddress;
        this.quantity = quantity;
        functionCode = FuncitonCodeEnum.READ_DISCRETE_INPUTS.toInt();
        expected = ExpectedEnum.READ_DISCRETE_INPUTS_EXPECTED.getValue();
    }

}
