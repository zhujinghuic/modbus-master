package com.function.base.write;

import com.response.ResponseData;

import java.util.List;

/**
 * 单写状态地址功能基础类
 * @author zhujh
 */
public abstract class BaseWriteStatusFunctionAction extends BaseWriteFunctionAction {


    @Override
    public void validateSendDataParams() {
        super.validDateStartAddress(writingAddress);
        this.validDateValue(value);
    }

    public void validDateValue(int value) {
        if (value < 0 && value > 1) {
            throw new IllegalArgumentException("值只能是0或1");
        }
    }

    @Override
    public List<ResponseData> getResPonseData(byte[] response) {
        return null;
    }

}
