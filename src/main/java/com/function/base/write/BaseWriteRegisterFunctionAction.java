package com.function.base.write;

import com.response.ResponseData;

import java.util.List;

/**
 * 单写寄存器功能基础类
 * @author zhujh
 */
public abstract class BaseWriteRegisterFunctionAction extends BaseWriteFunctionAction {


    @Override
    public void validateSendDataParams() {
        super.validDateStartAddress(writingAddress);
        this.validDateValue(value);
    }

    public void validDateValue(int value) {
        if (value < -32768 && value > 32767) {
            throw new IllegalArgumentException("超出地址数范围：-32768 ~ 32767");
        }
    }

    @Override
    public List<ResponseData> getResPonseData(byte[] response) {
        return null;
    }

}
