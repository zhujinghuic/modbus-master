package com.function.base.write;

import com.exception.ModbusException;
import com.function.base.BaseFunctionAction;
import com.main.tcp.protocol.RequestFrame;

/**
 * 批量写功能基础类
 * @author zhujh
 */
public abstract class BaseMultipleWriteFunctionAction extends BaseFunctionAction {

    protected int writingAddress;

    protected int[] values;

    public BaseMultipleWriteFunctionAction(){}


    @Override
    public void validateResponse(byte[] response) throws ModbusException {
        super.validateResponseByExpected(response, expected);
    }


}
