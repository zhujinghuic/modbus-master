package com.function.base.write;

import com.exception.ModbusException;
import com.function.base.BaseFunctionAction;
import com.main.tcp.protocol.RequestFrame;

/**
 * 单写功能基础类
 * @author zhujh
 */
public abstract class BaseWriteFunctionAction extends BaseFunctionAction {

    protected int writingAddress;

    protected int value;

    public BaseWriteFunctionAction(){}


    @Override
    public byte[] getReQuestByte() {
        RequestFrame requestFrame = getRequestFrameByFunction()
                .appendData(writingAddress)
                .appendData(value);
        return requestFrame.toRequest();
    }

    @Override
    public void validateResponse(byte[] response) throws ModbusException {
        super.validateResponseByExpected(response, expected);
    }


}
