package com.function.base.read;

import com.response.ResponseData;

import java.util.ArrayList;
import java.util.List;

/**
 * 读状态地址功能基础类
 * @author zhujh
 */
public abstract class BaseReadStatusFunctionAction extends BaseReadFunctionAction {



    @Override
    public List<ResponseData> getResPonseData(byte[] response) {
        List<ResponseData> resultList = new ArrayList<>();
        for (int i = 0; i < quantity; i++) {
            int value = response[RESPONSE_HEADER_LENGTH + i / 8];
            int mask  = (int)Math.pow(2, i % 8);
            int rpValue = ((value & mask) / mask) > 0 ? 1 : 0;
            resultList.add(new ResponseData(startAddress + i, rpValue));
        }
        return resultList;
    }

}
