package com.function.base.read;

import com.exception.ModbusException;
import com.function.base.BaseFunctionAction;
import com.main.tcp.protocol.RequestFrame;

/**
 * 读功能基础类
 * @author zhujh
 */
public abstract class BaseReadFunctionAction extends BaseFunctionAction {

    /** 开始读的地址 **/
    protected int startAddress;

    /** 读的数量 **/
    protected int quantity;

    public BaseReadFunctionAction(){
        super.LENGTH = 0x06;
    }

    @Override
    public void validateSendDataParams() {
        super.validDateStartAddress(startAddress);
        this.validDateQuantity(quantity);
    }

    @Override
    public byte[] getReQuestByte() {
        RequestFrame requestFrame = getRequestFrameByFunction()
                .appendData(startAddress)
                .appendData(quantity);
        return requestFrame.toRequest();
    }

    @Override
    public void validateResponse(byte[] response) throws ModbusException {
        super.validateResponseByExpected(response, expected);
    }


    protected void validDateQuantity(int quantity) {
        if (quantity > 2000 || quantity < 0) {
            throw new IllegalArgumentException("超出地址数范围：0~2000");
        }
    }

}
