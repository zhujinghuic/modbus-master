package com.function.base.read;

import com.response.ResponseData;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

/**
 * 读寄存器功能基础类
 * @author zhujh
 */
public abstract class BaseReadRegisterFunctionAction extends BaseReadFunctionAction {


    @Override
    public List<ResponseData> getResPonseData(byte[] response) {
        List<ResponseData> resultList = new ArrayList<>();
        for (int i = 0; i < quantity; i++) {
            int result = ByteBuffer.wrap(new byte[] {
                    response[RESPONSE_HEADER_LENGTH + i * 2],
                    response[RESPONSE_HEADER_LENGTH + i * 2 + 1],
            }).order(ByteOrder.BIG_ENDIAN).getShort();
            resultList.add(new ResponseData(startAddress + i, result));
        }

        return resultList;
    }

}
