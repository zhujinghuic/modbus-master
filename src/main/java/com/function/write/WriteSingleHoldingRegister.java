package com.function.write;

import com.constant.ExpectedEnum;
import com.constant.FuncitonCodeEnum;
import com.function.base.write.BaseWriteRegisterFunctionAction;
import com.response.ResponseData;

import java.util.List;

public class WriteSingleHoldingRegister extends BaseWriteRegisterFunctionAction {

//    protected static final int FUNCTION_CODE = 0x1;

    public WriteSingleHoldingRegister(int writingAddress, int value) {
        this.writingAddress = writingAddress;
        this.value = value;
        this.functionCode = FuncitonCodeEnum.WRITE_SINGLE_REGISTER.toInt();
        this.expected = ExpectedEnum.WRITE_SINGLE_REGISTER_EXPECTED.getValue();
    }

}
