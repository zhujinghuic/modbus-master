package com.function.write;

import com.constant.ExpectedEnum;
import com.constant.FuncitonCodeEnum;
import com.function.base.read.BaseReadStatusFunctionAction;
import com.function.base.write.BaseWriteStatusFunctionAction;
import com.response.ResponseData;

import java.util.List;

public class WriteSingleCoil extends BaseWriteStatusFunctionAction {

//    protected static final int FUNCTION_CODE = 0x1;

    public WriteSingleCoil(int writingAddress, int value) {
        this.writingAddress = writingAddress;
        this.value = value;
        this.functionCode = FuncitonCodeEnum.WRITE_SINGLE_COIL.toInt();
        this.expected = ExpectedEnum.WRITE_SINGLE_COIL_EXPECTED.getValue();
        this.value = value == 1 ? 0xFF00 : 0x0000;
    }

}
