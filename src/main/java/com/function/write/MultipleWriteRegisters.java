package com.function.write;

import com.constant.ExpectedEnum;
import com.constant.FuncitonCodeEnum;
import com.function.base.write.BaseWriteMultipleRegisterFunctionAction;

public class MultipleWriteRegisters extends BaseWriteMultipleRegisterFunctionAction {

//    protected static final int FUNCTION_CODE = 0x1;

    public MultipleWriteRegisters(int writingAddress, int[] values) {
        this.writingAddress = writingAddress;
        this.values = values;
        this.functionCode = FuncitonCodeEnum.WRITE_MULTIPLE_REGISTERS.toInt();
        this.expected = ExpectedEnum.WRITE_MULTIPLE_REGISTERS_EXPECTED.getValue();
    }

}
