package com.function.write;

import com.constant.ExpectedEnum;
import com.constant.FuncitonCodeEnum;
import com.function.base.write.BaseWriteMultipleStatusFunctionAction;
import com.response.ResponseData;

import java.util.List;

public class MultipleWriteCoils extends BaseWriteMultipleStatusFunctionAction {

//    protected static final int FUNCTION_CODE = 0x1;

    public MultipleWriteCoils(int writingAddress, int[] values) {
        this.writingAddress = writingAddress;
        this.values = values;
        this.functionCode = FuncitonCodeEnum.WRITE_MULTIPLE_COILS.toInt();
        this.expected = ExpectedEnum.WRITE_MULTIPLE_COILS_EXPECTED.getValue();
        initValues();
    }

    private void initValues() {
        for (int i = 0; i < values.length; i++) {
            this.values[i] = values[i] == 1 ? 0xFF00 : 0x0000;
        }
    }

}
