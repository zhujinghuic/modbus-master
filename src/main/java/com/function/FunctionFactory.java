package com.function;

import com.constant.FuncitonCodeEnum;
import com.function.base.BaseFunctionAction;
import com.function.read.ReadCoils;
import com.function.read.ReadDiscreteInputs;
import com.function.read.ReadHoldingRegisters;
import com.function.read.ReadInputRegisters;
import com.function.write.MultipleWriteCoils;
import com.function.write.MultipleWriteRegisters;
import com.function.write.WriteSingleCoil;
import com.function.write.WriteSingleHoldingRegister;

/**
 * 功能码工厂类
 */
public class FunctionFactory {

    private FunctionFactory() {

    }

    static private class SingletonHolder {
        final static private FunctionFactory instance = new FunctionFactory();
    }

    static public FunctionFactory getInstance() {
        return SingletonHolder.instance;
    }

    public BaseFunctionAction createMessage(int functionCode, int startAddress, Object value) {
        BaseFunctionAction function = null;

        switch (FuncitonCodeEnum.get(functionCode)) {
            case READ_COILS:
                function = new ReadCoils(startAddress, (int)value);
                break;
            case READ_DISCRETE_INPUTS:
                function = new ReadDiscreteInputs(startAddress, (int)value);
                break;
            case READ_HOLDING_REGISTERS:
                function = new ReadHoldingRegisters(startAddress, (int)value);
                break;
            case READ_INPUT_REGISTERS:
                function = new ReadInputRegisters(startAddress, (int)value);
                break;
            case WRITE_SINGLE_COIL:
                function = new WriteSingleCoil(startAddress, (int)value);
                break;
            case WRITE_SINGLE_REGISTER:
                function = new WriteSingleHoldingRegister(startAddress, (int)value);
                break;
            case WRITE_MULTIPLE_COILS:
                function = new MultipleWriteCoils(startAddress, (int[])value);
                break;
            case WRITE_MULTIPLE_REGISTERS:
                function = new MultipleWriteRegisters(startAddress, (int[])value);
                break;
            default:
                
        }
        return function;
    }
}
