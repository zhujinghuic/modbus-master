package com.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class JsonResult {
    private int responseCode;
    private boolean status;
    private List<ResponseData> data;

    public static JsonResult getSuccessResult(List<ResponseData> data) {
        return new JsonResult(200, true, data);
    }

    public static JsonResult getSuccessResult() {
        return new JsonResult(200, true, null);
    }

    public static JsonResult getErrorResult(List<ResponseData> data) {
        return new JsonResult(500, false, data);
    }

    public static JsonResult getErrorResult() {
        return new JsonResult(500, false, null);
    }
}
