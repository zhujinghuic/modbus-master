package com;


import com.main.tcp.ModbusTcpClient;

/**
 * client实例工厂
 * @author zhujh
 */
final public class ModbusClientFactory {

    private ModbusClientFactory() {}


//    static public ModbusTcpClient createModbusMasterRTU(SerialParameters sp) throws SerialPortException {
//        return new ModbusMasterRTU(sp);
//    }


//    static public ModbusMaster createModbusMasterASCII(SerialParameters sp) throws SerialPortException {
//        return new ModbusMasterASCII(sp);
//    }


    static public ModbusTcpClient createModbusMasterTCP(String modbusIp, int port) {
        return new ModbusTcpClient(modbusIp, port);
    }
}
