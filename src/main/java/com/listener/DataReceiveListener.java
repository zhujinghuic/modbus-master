package com.listener;

/**
 * 接收功能handler拓展接口
 * @author zhujh
 */
public interface DataReceiveListener {
    void onDataReceive(byte[] dataReceive);
}