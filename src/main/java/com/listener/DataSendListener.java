package com.listener;

/**
 * 发送功能handler拓展接口
 * @author zhujh
 */
public interface DataSendListener {
    void onDataSend(byte[] dataSend);
}