package com.constant;

/**
 * 各功能码expected
 * @author zhujh
 */
public enum ExpectedEnum {
    READ_COILS_EXPECTED(0x81),
    READ_DISCRETE_INPUTS_EXPECTED(0x82),
    READ_HOLDING_REGISTERS_EXPECTED(0x83),
    READ_INPUT_REGISTERS_EXPECTED(0x84),
    WRITE_SINGLE_COIL_EXPECTED(0x85),
    WRITE_SINGLE_REGISTER_EXPECTED(0x86),
    WRITE_MULTIPLE_COILS_EXPECTED(0x8F),
    WRITE_MULTIPLE_REGISTERS_EXPECTED(0x90);


    final private int value;

    ExpectedEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
