package com.constant;

/**
 * 常量类
 * @author zhujh
 */
final public class ModbusConstant {

    final static public String MODBUS_IP = "127.0.0.1";
    final static public int PORT = 502;
    final static public int PROTOCOL_ID = 0;
    final static public int SERVER_ADDRESS = 1;
    final static public int transactionId = 0;
    final static public int TRANSACTION_ID_MAX_VALUE = 0xFFFF;
    final static public int DEFAULT_READ_TIMEOUT = 1000;
    final static public int CONNECT_TIMEOUT = 1000;

}
