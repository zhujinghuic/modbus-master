package com;

import com.response.JsonResult;
import com.exception.ModbusException;

import java.io.IOException;

public interface ModbusFunctionInterface {

    JsonResult readCoils (int startAddress, int quantity) throws ModbusException, IOException;

    JsonResult readDiscreteInputs (int startAddress, int quantity) throws ModbusException, IOException;

    JsonResult readHoldingRegisters (int startAddress, int quantity) throws IOException, ModbusException;

    JsonResult readInputRegisters(int startAddress, int quantity) throws IOException, ModbusException;

    JsonResult writeCoil(int startingAddress, int value) throws IOException, ModbusException;

    JsonResult writeHoldingRegister(int startingAddress, int value) throws IOException, ModbusException;

    JsonResult multipleWriteCoils(int startingAddress, int[] values) throws IOException, ModbusException;

    JsonResult multipleWriteHoldingRegisters(int startingAddress, int[] value) throws IOException, ModbusException;

}
